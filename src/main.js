import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
// You can change this import to `import router from './starterRouter'` to quickly start development from a blank layout.
import router from './router';
import NowUiKit from './plugins/now-ui-kit';
import axios from 'axios';
import jquery from 'jquery';

Vue.config.productionTip = false;
Vue.config.devtools = false;
Vue.prototype.$API = 'https://hippos.dakitec.de/api/';
Vue.prototype.$http = axios;
Vue.prototype.$jquery = jquery;
Vue.use(NowUiKit);
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: {},
    subjects: {}
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
