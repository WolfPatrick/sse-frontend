import Vue from 'vue';
import Router from 'vue-router';
import Index from './pages/Index.vue';
import Landing from './pages/Landing.vue';
import Login from './pages/Login.vue';
import Profile from './pages/Profile.vue';
import MainNavbar from './layout/MainNavbar.vue';
import LoginNavbar from './layout/LoginNavbar.vue';
import Overview from './pages/Overview.vue';
import Grades from './pages/Grades.vue';
import Exams from './pages/Exams.vue';
import Search from './pages/Search.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      components: { default: Index, header: MainNavbar },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: '/landing',
      name: 'landing',
      components: { default: Landing, header: MainNavbar },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: '/search',
      name: 'search',
      components: { default: Search, header: MainNavbar },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: '/login',
      name: 'login',
      components: { default: Login, header: LoginNavbar },
      props: {
        header: { colorOnScroll: 500, transparent: false }
      }
    },
    {
      path: '/overview',
      name: 'overview',
      components: { default: Overview, header: MainNavbar },
      props: {
        header: { colorOnScroll: 500, transparent: false }
      }
    },
    {
      path: '/grades',
      name: 'grades',
      components: { default: Grades, header: MainNavbar },
      props: {
        header: { colorOnScroll: 500, transparent: false }
      }
    },
    {
      path: '/exams',
      name: 'exams',
      components: { default: Exams, header: MainNavbar },
      props: {
        header: { colorOnScroll: 500, transparent: false }
      }
    },
    {
      path: '/profile',
      name: 'profile',
      components: { default: Profile, header: MainNavbar },
      props: {
        header: { colorOnScroll: 400 }
      }
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
